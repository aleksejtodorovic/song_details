export const SONG_SELECTED = 'SONG_SELECTED';

export const selectSong = song => ({
    type: SONG_SELECTED,
    payload: song
});