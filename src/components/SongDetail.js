import React from 'react';
import { connect } from 'react-redux';

const SongDetail = ({ song }) => {
    if (!song) {
        return (
            <div className="ui segment">
                Please select a song
                </div>
        );
    }

    return (
        <div className="ui segment">
            <h2>Title: {song.title}</h2>
            <h2>Duration: {song.duration}</h2>
        </div>
    );
}

const mapStateToProps = ({ selectedSong }) => ({
    song: selectedSong
});

export default connect(mapStateToProps)(SongDetail);