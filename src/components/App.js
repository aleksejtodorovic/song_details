import React from 'react';
import SongList from './SongList';
import SongDetail from './SongDetail';

class App extends React.Component {
    render() {
        return (
            <div className="ui grid container" style={{ marginTop: '20px' }}>
                <div className="row">
                    <div className="eight wide column">
                        <SongList />
                    </div>
                    <div className="eight wide column">
                        <SongDetail />
                    </div>
                </div>
            </div>
        );
    }
}

export default App;