import React from 'react';
import { connect } from 'react-redux';
import SongItem from './SongItem';

class SongList extends React.Component {
    renderList() {
        return this.props.songs.map(song => {
            return (
                <SongItem key={song.title} song={song} />
            );
        });
    }

    render() {
        return (
            <div className="ui middle aligned divided list">
                {this.renderList()}
            </div>
        );
    }
}


const mapStateToProps = ({ songs }) => ({
    songs
});

export default connect(mapStateToProps)(SongList);