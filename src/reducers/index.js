import { combineReducers } from 'redux';
import { SongListReducer } from './reducer_song_list';
import { SongSelectedReducer } from './reducer_selected_song';

export default combineReducers({
    songs: SongListReducer,
    selectedSong: SongSelectedReducer
});