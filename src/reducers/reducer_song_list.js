const allSongs = [
    {
        title: 'First song',
        duration: '2.56'
    },
    {
        title: 'Second song',
        duration: '3.55'
    },
    {
        title: 'Third song',
        duration: '4.25'
    }
];

export const SongListReducer = () => (allSongs);