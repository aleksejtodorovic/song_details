import { SONG_SELECTED } from '../actions';


export const SongSelectedReducer = (state = null, action) => {
    switch (action.type) {
        case SONG_SELECTED:
            return action.payload
        default:
            return state;
    }
};